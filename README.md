# This is not working anymore because twitch shut down the old API 😢

# Description
This is a simple and crappy python program that checks your followed channels in twitch.tv  and shows you which ones are live.
Then you can select one of them and it will use streamlink to play it.
It needs some more work... :)

# Installation
pip install twitchercli

# Requirements
* python 3.6 (Probably works on lower ones but I haven't test it.)
* See requirements.txt for python modules
* streamlink (You probably need to add you twitch OAuth token in it's config.)

# Usage
Just type `twitchercli` in your terminal and when asked choose the number of the stream you want to watch or ctrl+c to
exit.

# Additional Notes
You **need** to add a clientID **and** a oauth token for `twitchercli` to function!  
If there is a problem running it make sure you have the dependancies installed and check the config file for mistakes.
You can create a new example config file by deleting the twitchercli folder in `~/.config/`, do not forget to add a
clientID and your twitch OAuth token.  

