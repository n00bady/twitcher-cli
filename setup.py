import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

deps = [
        "requests>=2.23.0",
        "streamlink>=1.3.1"
]

setuptools.setup(
        name="twitchercli",
        version="0.1.2",
        author="n00bady",
        author_email="kaz00@vivaldi.net",
        description="A simple cli tool to keep track of your favorite twitch.tv streamers",
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="https://gitlab.com/n00bady/twitcher-cli",
        packages=setuptools.find_packages(),
        classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
            "Operating System :: OS Independent",
        ],
        install_requires=deps,
        python_requires='>=3.6',
        entry_points = {
            'console_scripts': [
                'twitchercli=twitchercli.twitcher:main',
                ],
            }
)
