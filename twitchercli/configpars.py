import sys
import os
import configparser
from pathlib import Path

# Get configuration or create an example one
# Should I break it up and put in a different functions the example config creations and the check for Client and OAuth ?
def getconfig():
    # Get home folder path
    home = str(Path.home())
    # Check if confi.cfg file exists
    # TODO: Sell my soul to Jeff to get a ClientID :-|
    try:
        with open(home+'/.config/twitchercli/config.ini', 'r') as conf:
            # Load configuration
            cfg = configparser.ConfigParser(inline_comment_prefixes="#") # Inline prefixes can be dangerous !
            cfg.read(home+'/.config/twitchercli/config.ini')
            # Check if ClientID and OAuth exists
            if cfg.get('Settings', 'ClientID') == '' or cfg.get('Settings', 'OAuth') == '':
                print('ClientID or OAuth token not found in config file !')
                print('You NEED a valid ClientID AND a valid OAuth !')
                print('The configuration file is located at ~/.config/twitchercli/config.ini')
                sys.exit()
            return cfg
    except FileNotFoundError:
        # Warn the user about none existing configuration file
        # And then create an example config file
        print('No configuration file found !')
        print('Creating and example configuration file at ~/.config/twitchercli/config.ini')
        print('Please make sure to add your ClientID and OAuth token ! ! !')
       
        # I probably should break this up to a different function or use another way to write it in one go.
        os.mkdir(home+'/.config/twitchercli/')
        cfgfile = open(home+'/.config/twitchercli/config.ini', 'w')
        cfg = configparser.ConfigParser()
        cfg.add_section('Settings')
        cfg.set('Settings', 'ClientID', 'u2zl5o4o44pmkxh7mmhalicn1w1u3i')
        cfg.set('Settings', 'OAuth', '# Put your OAuth token here !')
        cfg.set('Settings', 'alt_line', '237')
        cfg.set('Settings', 'line', '0')
        cfg.set('Settings', 'enable_chat', 'true')
        cfg.add_section('Streamlink')
        cfg.set('Streamlink', 'player', 'mpv')
        cfg.set('Streamlink', 'Quality', '720p')
        cfg.write(cfgfile)
        cfgfile.close()
        sys.exit()

