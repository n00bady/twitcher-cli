import requests

# GET request from the twitch v5 api
def jason(clientID, oauth):
    # Headers, need to specify that we use the v5 api and must send a client-id
    # and a OAuth
    headers = {'Accept': 'application/vnd.twitchtv.v5+json',
            'Client-ID': clientID,
            'Authorization': 'OAuth '+oauth}

    r = requests.get('https://api.twitch.tv/kraken/streams/followed',
            headers=headers)
    # Check response status code
    if r.status_code == 200:
        res = r.text
        return res
    else:
        # I am not sure if I should end the program like that from here...
        print('Somethings is wrong!')
        print("The response's status code was: ", r.status_code, '\n')
        r.raise_for_status()

