import sys
import json
import subprocess
import webbrowser
import signal
import configparser
import os

from .configpars import getconfig
from .twitchjason import jason

# Print channels with their name game and current viewer count
# Should this moved to a separate module ?
def printchannels(data, cfg):
    # I assume that the terminal will have at least 80 columns. It might work
    # for smaller but sometimes if the big streamer names or games break it...
    # TODO: Fix the table when it has to deal with smaller than 80 columns.
    totalStreams = len(data['streams'])
    #dash = '-' * 80
    alt_line_color = cfg.get('Settings', 'alt_line')
    line_color = cfg.get('Settings', 'line')

    # Stuff for the initial first f string
    ind = 'Int'
    streamer = 'Streamer'
    game = 'Game'
    views = 'Viewers'

    # Figure out the current terminal dimensions 
    col, lin = os.get_terminal_size()
    extra = (col - 80)//2
    dash = '-' * col

    print('Checking', totalStreams, 'streams...\n')
    print(dash)
    print(f'|{ind:<4}|{streamer:<{29+extra}}|{game:<{34+extra}}|{views:<8}|')
    print(dash)
    for i in range(0, totalStreams):
        channelName = data['streams'][i]['channel']['display_name']
        channelGame = data['streams'][i]['channel']['game']
        channelViewers = str(data['streams'][i]['viewers'])
        live = data['streams'][i]['stream_type']
        # If the streamer name or the game title is too long truncate them and replace the last 3 chars with dots
        channelName = (channelName[:26+extra]+'...') if len(channelName) >= 26+extra else channelName
        channelGame = (channelGame[:31+extra]+'...') if len(channelGame) >= 31+extra else channelGame
        # Every other line will have a different background color
        if i % 2 == 0:
            print(f'\033[48;5;{alt_line_color:<0}m|{i:<4}|{channelName:<{29+extra}}|{channelGame:<{34+extra}}|{channelViewers:>8}|\033[0m')
        else:
            print(f'\033[48;5;{line_color:<0}m|{i:<4}|{channelName:<{29+extra}}|{channelGame:<{34+extra}}|{channelViewers:>8}|\033[0m')
    print(dash)

# Get user selection and return the channelName
def getUserSelection(data):
    # Handling EOF when user inputs Ctrl+D
    try:
        selection = int(input("\033[1;32mSelect >>\033[0m "))
    except EOFError:
        print("\nPlease don't press ctrl+d again T_T ")
        sys.exit()
    # In case the user enters nothing or non int value he gets memed
    except ValueError:
        print('You get nothing! You lose! Good day, sir!')
        sys.exit()
    channelName = data['streams'][selection]['channel']['display_name']
    channelTitle = data['streams'][selection]['channel']['status']
    print('Playing: ', channelTitle)    # These don't belong here but whatever ¯\_(ツ)_/¯
    print('From: ',  channelName)
    print('-' *40)
    return channelName

# Handling Ctrl+C
def keyboardInterruptHandler(signal, fram):
    print("\nKeyboardInterrupt! Exiting...")
    sys.exit()


def main():
    # Handle Ctrl+C
    signal.signal(signal.SIGINT, keyboardInterruptHandler)

    # Get configuration file
    cfg = getconfig()

    # Get a the json response from the twitch api
    # Putting all that in json.loads() doesn't seem to work even when I use the cfg.get()
    response = jason(cfg['Settings']['ClientID'], cfg['Settings']['OAuth'])
    data = json.loads(response)
    # Need to pass the configurations so I can get the values for the line colors
    printchannels(data, cfg)
    sel = getUserSelection(data)

    # Open popout chat in a new tab on default browser
    if cfg.getboolean('Settings', 'enable_chat'):
        # There are some problems with this when the browser is not already open, maybe check if a browser is open first ?
        print('Opening popout chat on default browser...')
        # Opening a new tab on default browser for the popoutchat
        chaturl = 'https://www.twitch.tv/popout/'+sel+'/chat'
        webbrowser.open_new_tab(chaturl)

    # Send the selection to streamlink to play the stream
    print('Opening stream through streamlink...')
    subprocess.run(['streamlink', '-p', cfg.get('Streamlink', 'player'), '--twitch-oauth-token', cfg.get('Settings',
        'OAuth'), 'https://twitch.tv/'+sel, cfg.get('Streamlink', 'Quality')]) 

if __name__ == "__main__":
    main()
