# TODO List

- [x] Make it interactive so you can choose one of the streams displayed.
- [x] Send the selection to streamlink to watch the stream.
- [x] Make it possible to configure how the streamlink will play the stream.
- [x] Find a better way to handle and pass around the config values
- [x] Find a way to open a popout chat for the stream.
- [x] Add the ability to configure it through a config file.
- [x] PyPI package structure.
- [ ] Add more features.
- [x] Add a request response check.
- [x] Make the printed results prettier!
- [x] Make the colors configurable from the config file.
- [x] Fix the table breakage when an element has a lot of charachters.
- [ ] Find a way to update old config files.
- [ ] Maybe add configuration defaults inside the code so when config options are missing it wont stall.
- [ ] Add the ability to scale the output acording to the terminal dimensions.
- [ ] Maybe add a non interactive mode that will use arguments to function.
- [ ] Make the code cleaner and prettier. (This will probably never happen...)
- [ ] Maybe make the output and it's formating a different local module and make it handle **all** output.
- [ ] Make it trully OS agnostic.
- [x] Give my phone number to Jeff to get a legit clientID :S
- [ ] ???
- [ ] Don't profit!

